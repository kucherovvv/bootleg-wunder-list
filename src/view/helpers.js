
/**
 * 
 * @param  {...any} styles 
 */
export const mergeStyles = (...styles) => {
  const resultStyle = {};
  for (let i = 0; i < styles.length; i++) {
    if (resultStyle)
      Object.assign(styles, resultStyle)
  }
  return resultStyle;
}

/**
 * add together names of classes, skipping falsy values
 */
export const mergeClasses = (...classes) => classes.filter(Boolean).join(' ');