
import React from 'react';
import { mergeClasses } from '../helpers';

export default (props) => (
  <button {...props} onClick={props.onClick} className={mergeClasses(props.className, 'btn', (props.type && 'btn-'+props.type))}>
    {props.children}
  </button>
)