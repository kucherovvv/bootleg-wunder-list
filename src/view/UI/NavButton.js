
import React from 'react';
import { NavLink } from 'react-router-dom';
import { mergeClasses } from '../helpers';

export default (props) => (
  <NavLink to={props.to} className={mergeClasses(props.className, 'btn', (props.type && 'btn-'+props.type)) }>
    {props.children}
  </NavLink>
)