import React from 'react';
import {mergeClasses} from '../helpers';

const contentStyle = {
  flex:'1 1 0',
  paddingTop:10,
  paddingBottom:10,
  overflow:'auto',
  zIndex:0
}

export default (props) => (
  <div className={mergeClasses('bg-light container-fluid', props.className)} style={contentStyle}>
    {props.children}
  </div>
)