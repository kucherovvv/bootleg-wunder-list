import React from 'react';
import {Button} from "../UI";
import Bar from "./Bar";
import {withRouter} from 'react-router';

export default withRouter(({history, children}) => (
  <Bar>
    <Button onClick={history.goBack} type='secondary'>Go back</Button>
    {children}
  </Bar>
))