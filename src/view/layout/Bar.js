import React from 'react';

const barStyle = {
  display: 'flex',
  
}

export default (props) => (
  <div className='navbar bg-dark text-light clearfix' style={barStyle}>
    {props.children}
  </div>
)