import React from 'react';
import {Transition} from 'react-transition-group';

const pageStyle = {
  display: 'flex',
  flexDirection: 'column',
  height:'100vh',
  width:'100vw'
}


export default (props) => (
  <div style={pageStyle}>
    {props.children}
  </div>
)