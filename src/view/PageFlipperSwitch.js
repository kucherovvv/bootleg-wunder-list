import React from 'react';
import {matchPath, withRouter} from 'react-router';

class PageFlipperSwitch extends React.Component {
  static defaultProps = {
    duration: 150,
    timingFunction:'ease-in-out'
  }

  state = {
    tweeningPair: [], //one that's tweening in, one that's tweening out
    currentKey: null,
    timerId: null
  }

  constructor(props) {
    super(props)
  }

  componentDidMount() {
    this.doTheThing(this.props);
  }

  componentWillReceiveProps(nextProps) {
    this.doTheThing(nextProps);
  }

  doTheThing(nextProps) {
    // console.log('yee')
    const {location, match, history} = nextProps;
    let { children } = nextProps;
    if (!Array.isArray(children)) children = [children];

    // debugger;
    const currentKey = children.findIndex((child) => {
      if (child.props.animpath==null) return true;
      return Boolean(matchPath(location.pathname, {exact: child.props.exact, path:child.props.animpath}));
      // return (match && match.isExact === Boolean(child.props.exact))
    });

    if (this.state.currentKey !== currentKey) {
      if (this.state.timerId) {
        clearTimeout(this.state.timerId);
      }
      const tweeningPair = [currentKey, this.state.currentKey];
      console.log(`tweening [${tweeningPair}]`)
      const timerId = setTimeout(() => {
        console.log('tween stop')
        this.setState({
          tweeningPair: [],
          timerId: null
        });
      }, nextProps.duration);

      this.setState({
        tweeningPair,
        currentKey,
        timerId
      });
    }
  }



  componentWillUnmount() {
    if (this.state.timerId) {
      clearTimeout(this.state.timerId);
    }
  }

  render() {
    let { children } = this.props;
    if (!Array.isArray(children)) children = [children];
    const { duration, timingFunction } = this.props;
    const {tweeningPair: [tweeningInKey, tweeningOutKey], currentKey} = this.state;
    const tweens = {
      fromRight: `page-tweening-right ${duration}ms 1 ${timingFunction} forwards reverse`,
      toRight: `page-tweening-right ${duration}ms 1 ${timingFunction} forwards`,
      fromLeft: `page-tweening-left ${duration}ms 1 ${timingFunction} forwards reverse`,
      toLeft: `page-tweening-left ${duration}ms 1 ${timingFunction} forwards`,
    };

    const commonStyle = {
      position:'absolute',
      top:0
    }
    // if (children.length===1) {
    //   commonStyle.left = 100;
    //   commonStyle.boxShadow = '0 0 20px rgba(0,0,0,.3)';
    // }
    const tweeningOutStyle = {
      ...commonStyle,
      pointerEvents:'none',
      animation: tweeningOutKey==null || tweeningInKey==null || tweeningOutKey==-1 || tweeningInKey==-1 || tweeningOutKey > tweeningInKey ? tweens.toRight : tweens.toLeft
    };
    const tweeningInStyle = {
      ...commonStyle,
      pointerEvents:'none',
      animation: tweeningOutKey==null || tweeningInKey==null || tweeningOutKey==-1 || tweeningInKey==-1 || tweeningOutKey < tweeningInKey ? tweens.fromRight : tweens.fromLeft
    };
    

    return (
      children.map((child, i) => (
        i === tweeningOutKey
        ? <div key={i} style={tweeningOutStyle}>{child}</div>
        : i === tweeningInKey
          ? <div key={i} style={tweeningInStyle}>{child}</div>
          : i === currentKey
            ? <div key={i} style={commonStyle}>{child}</div>
            : false
      ))
    )
  }
}

// function cloneAndModifyChild(child) {
//   return child//React.cloneElement(child, {...child.props, path:undefined}); // sad thing is that it causes the page to render twice
// }

export default withRouter(PageFlipperSwitch);