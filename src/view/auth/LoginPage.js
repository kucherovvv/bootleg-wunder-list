import React from 'react';
import { Button, NavButton } from '../UI';
import { Page, Content, Bar } from '../layout';
import { Route, Redirect } from 'react-router-dom';
import firebaseApp from '../../apis/firebaseApp';
import mainLogo from '../../img/main-logo.png'
import SignupPage from './SignupPage';
import PageFlipperSwitch from '../PageFlipperSwitch';

export default class LoginPage extends React.Component {

  state = {
    isFetching:false,
    error: ''
  }

  constructor(props) {
    super(props);

    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount() {
    if (firebaseApp.auth().currentUser) this.props.onLoggedIn();
  }

  async handleSubmit(e) {
    e.preventDefault();
    if (this.state.isFetching) return;

    const buttonName = e.currentTarget.name;
    
    const email = e.currentTarget.email.value;
    const password = e.currentTarget.password.value;

    this.setState({isFetching:true, error:''});
    await new Promise(r=>setTimeout(r,2000));
    const response = 
      await firebaseApp.auth().signInWithEmailAndPassword(email, password)
      .then((response) => {
        console.log(response);
        this.setState({isFetching:false, error:''});
      }, (e) => {
        console.error(e);
        this.setState({isFetching:false, error: e.message || e})
      })
  }

  render() {
    const { isFetching, error } = this.state;

    return (
      // <>
      <Page>
        <Bar>Esketit</Bar>
        <Content className='text-center'>
          <form onSubmit={this.handleSubmit} className='bg-light text-dark text-center' style={{display:'inline-block'}}>
            <div>
              <img alt='' src={mainLogo}/>
            </div>
            <div>
              <span>Login:</span>
            </div>
            <div>
              <input name='email'/>
            </div>
            <div>
              <span>Password:</span>
            </div>
            <div>
              <input type='password' name='password'/>
            </div>
            <div className='btn-group'>
              <Button name='login' type='success'>Login</Button>
              <NavButton to='/auth/signup' type='info'>Sign up</NavButton>
            </div>
            <div>
              {
                isFetching
                ? <span>Waiting...</span>
                : error 
                  ? <span>Error: {error}</span>
                  : false
              }
            </div>
          </form>

        </Content>
        <Bar>Yeet</Bar>
      </Page>

      
      /* <PageFlipperSwitch duration={150} >
        <Route animpath='/auth/signup' render={()=><SignupPage onLoginSuccess={this.props.onLoginSuccess}/>}/>
      </PageFlipperSwitch> */
      /* </> */
    )
  }
}