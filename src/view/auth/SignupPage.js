import React from 'react';
import { Button, NavButton } from '../UI';
import { Page, Content } from '../layout';
import { Redirect } from 'react-router-dom';
import firebaseApp from '../../apis/firebaseApp';
import mainLogo from '../../img/main-logo.png'
import NavBackBar from '../layout/NavBackBar';

export default class SignupPage extends React.Component {

  state = {
    isFetching:false,
    error:''
  }

  constructor(props) {
    super(props);

    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount() {
    if (firebaseApp.auth().currentUser) this.props.onLoggedIn();
  }

  async handleSubmit(e) {
    e.preventDefault();
    if (this.state.isFetching) return;

    const buttonName = e.currentTarget.name;
    
    const email = e.currentTarget.email.value;
    const password = e.currentTarget.password.value;

    this.setState({isFetching:true, error:''});
    await new Promise(r=>setTimeout(r,2000));
    const response = 
      await firebaseApp.auth().createUserWithEmailAndPassword(email, password)
      .then((response) => {
        console.log(response);
        this.setState({isFetching:false, error:''});
      }, (e) => {
        console.error(e);
        this.setState({isFetching:false, error: e.message || e})
      })
  }


  render() {
    const { isFetching, error } = this.state;

    return (
      <Page>
        <NavBackBar>
          Some unrelated text
        </NavBackBar>
        <Content className='text-center'>
          <form onSubmit={this.handleSubmit} className='bg-light text-dark text-center' style={{display:'inline-block'}}>
            <h1 className='bg-dark text-light'>
              Create new account
            </h1>
            <div>
              <span>Login:</span>
            </div>
            <div>
              <input name='email'/>
            </div>
            <div>
              <span>Password:</span>
            </div>
            <div>
              <input type='password' name='password'/>
            </div>
            <div>
              <span>Repeat password:</span>
            </div>
            <div>
              <input type='password' name='password-repeat'/>
            </div>
            <div className='btn-group'>
              <Button name='signup' type='success'>Create account</Button>
              <Button name='facebook' type='info'><i className='fa fa-facebook'></i></Button>
              <Button name='google' type='danger'><i className='fa fa-google'></i></Button>
            </div>
            <div>
              {
                isFetching
                ? <span>Waiting...</span>
                : error 
                  ? <span>Error: {error}</span>
                  : false
              }
            </div>
          </form>

        </Content>
      </Page>
    )
  }
}