import React from 'react';
import { Page, Bar, Content } from '../layout';
import { Button, NavButton } from '../UI';
import firebaseApp from '../../apis/firebaseApp';

export default class Lists extends React.Component {
  render() {
    return (
      <Page>
        <Bar><NavButton to='/' type='info'>Push plz</NavButton><i className='fa fa-facebook'></i></Bar>
        <Content>
          <div className="cards">
            <div className="cards__header">
              Lorem ipsum dolor sit amet consectetur adipisicing.
              <pre>
                {JSON.stringify(firebaseApp.auth().currentUser, null, 4)}
              </pre>
            </div>
            <div className="cards__body">
              <label className="cards__entry entry media">
                <div className="entry__side media__side">
                  <label className="checkbox"><input className="checkbox__input" type="checkbox"/><span className="checkbox__view"></span></label>
                </div>
                <div className="entry__content media__content">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim, nemo.</div>
              </label>
              <label className="cards__entry entry media">
                <div className="entry__side media__side">
                  <label className="checkbox"><input className="checkbox__input" type="checkbox"/><span className="checkbox__view"></span></label>
                </div>
                <div className="entry__content media__content">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim, nemo.</div>
              </label>
              <label className="cards__entry entry media">
                <label className="entry__side media__side">
                  <label className="checkbox"><input className="checkbox__input" type="checkbox"/><span className="checkbox__view"></span></label>
                </label>
                <div className="entry__content media__content">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim, nemo.</div>
              </label>
              <label className="cards__entry entry media">
                <div className="entry__side media__side">
                  <label className="checkbox"><input className="checkbox__input" type="checkbox"/><span className="checkbox__view"></span></label>
                </div>
                <div className="entry__content media__content">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim, nemo.</div>
              </label>
              <label className="cards__entry entry media">
                <div className="entry__side media__side">
                  <label className="checkbox"><input className="checkbox__input" type="checkbox"/><span className="checkbox__view"></span></label>
                </div>
                <div className="entry__content media__content">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim, nemo.</div>
              </label>
              <label className="cards__entry entry media">
                <div className="entry__side media__side">
                  <label className="checkbox"><input className="checkbox__input" type="checkbox"/><span className="checkbox__view"></span></label>
                </div>
                <div className="entry__content media__content">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim, nemo.</div>
              </label>
              <label className="cards__entry entry media">
                <div className="entry__side media__side">
                  <label className="checkbox"><input className="checkbox__input" type="checkbox"/><span className="checkbox__view"></span></label>
                </div>
                <div className="entry__content media__content">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim, nemo.</div>
              </label>
            </div>
          </div>
        </Content>
      </Page>
    
    )
  }
}