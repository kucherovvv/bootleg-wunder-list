import React from 'react';
import './App.css';
import {Page, Bar, Content} from './view/layout';
import {Button,NavButton} from './view/UI';
import PageFlipperSwitch from './view/PageFlipperSwitch';
import ListsPage from './view/lists/ListsPage';
import {withRouter} from 'react-router'; 
import {Switch, Route, Redirect} from 'react-router-dom'; 
import firebaseApp from './apis/firebaseApp';
// import AuthPageSwitcher from './view/auth/AuthPageSwitcher';
import SignupPage from './view/auth/SignupPage';
import LoginPage from './view/auth/LoginPage';



const initialState = {
  user: null,
}

class App extends React.Component {
  state = initialState

  constructor(props) {
    super(props);
    this.handleLoggedIn = this.handleLoggedIn.bind(this);
  }

  componentDidMount() {
    firebaseApp.auth().onAuthStateChanged((user) => {
      if (user) {
        this.props.history.push('/lists');
      } else {
        this.props.history.push('/auth/login');
      }
    })

    if (!this.props.location.pathname.startsWith('/auth'))
      this.props.history.push('/auth/login');

    
  }

  handleLoggedIn() {
    // this.setState({ user });
    console.log(this.props.history)
    this.props.history.push('/lists');
  }

  render() {

    return (
      <PageFlipperSwitch duration={150} >
        <Route exact animpath='/' render={() =>
          <Page>
            <Content className='text-center'>
              {/* {!this.state.user ? <Redirect to='/login'/> : false} */}

              <pre>{JSON.stringify(this.state.user, null, 4)}</pre>
              <div className="btn-group">
                <NavButton to='/asdasd' type='secondary'>Goto non-existing Page</NavButton>
                <NavButton to='/rando' type='warning'>Goto RandoPage</NavButton>
                <NavButton to='/auth/login' type='info'>Goto LoginPage</NavButton>
                <NavButton to='/lists' type='danger'>Goto ListsPage</NavButton>
                <Button onClick={()=>firebaseApp.auth().signOut()}>sign out</Button>
              </div>
            </Content>
          </Page>
        }/>
        <Route animpath='/rando' component={RandoPage}/>
        <Route animpath='/lists' component={ListsPage}/>
        <Route animpath='/auth/login' render={()=>(<LoginPage onLoggedIn={this.handleLoggedIn}/>)}/>
        <Route animpath='/auth/signup' render={()=>(<SignupPage onLoggedIn={this.handleLoggedIn}/>)}/>
          
      </PageFlipperSwitch>
      
    );
  }
}

const RandoPage = withRouter(class RandoPage extends React.Component {
  state = {
    picB64: null
  }

  constructor(props) {
    super(props);

    this.takePhoto = this.takePhoto.bind(this);
  }

  componentDidMount() {
    this.props.history.block('yee?')
  }


  takePhoto() {
    const camera = navigator.camera;
    const Camera = window.Camera
    camera.getPicture((data) => {
      this.setState({
        picB64: data
      });
      console.log(data)
    }, (message) => {
      console.warn(message);
    }, {
      destinationType: Camera.DestinationType.DATA_URL,
      cameraDirection: Camera.Direction.FRONT,
      saveToPhotoAlbum: false,
      sourceType: Camera.PictureSourceType.CAMERA,
      targetHeight: 100,
      targetWidth: 100
    })
  }

  render() {
    return (
      <Page>
        <Bar>My app 9001 </Bar>
        <Content>

          <Button type='info' onClick={this.takePhoto}>Take a photo </Button>
          {
            this.state.picB64
            ? <img alt='' src={'data:image/jpeg;base64,'+this.state.picB64}/>
            : <span>No photo taken yet</span>
          }
          <span  className='blockquote-footer'>Yee</span>
        </Content>
        <Bar> Bottom </Bar>
      </Page>
    )
  }
})


export default withRouter(App);
