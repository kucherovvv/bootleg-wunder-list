# This repo contains mostly configured cordova+react dev environment
## To get it working, do the following:
1. download and install node
2. install necessary shit
    * for android:
        1. download and install android studio
    * for ios: (didn’t try it myself, so it might be not 100% accurate)
        1. download and install *xcode* from App Store
        2. `xcode-select --install`
        3. `npm install -g ios-sim`
        4. `npm install -g ios-deploy`
3. launch command line in project directory
4. `npm install` – installs all necessary packages
5. `npm run start` – starts local dev server on http://localhost:3000 (also available on http://192.168.x.x:3000). Now when you edit files in /src, the server will instantly recompile the whole app, and all clients(ex: browser, cordova app) will see the changes.
6. ~~take a note of the address that looks like http://192.168.x.x:3000 in the command line window~~

    **Cordova time!**

7. ~~open */config.xml* and write there the address from the previous step~~  
`node ./prepare-a-little.js` - inits /www directory and puts your local ip into config.xml
8. `npm install cordova --global`
9. `cordova prepare` – cordova will download all necessary plugins and add platforms (*ios*, *android*, *browser*) 
10. `node ./steal-cordova-scripts.js` – more on that in the section for curios down below
11. turn on debugging via usb on your phone (google it), plug in your phone, probably gotta download and install driver for your phone
12. install dummy cordova app on your device
    * `cordova run android --device`
    or
    * `cordova run ios --device`
    During development you only have to do this once, so app is installed on the phone.

**


### Now when everything is installed, how do I start dev server next time?
![](https://i.makeagif.com/media/8-13-2016/vjFf6k.gif =100x)

1. `npm run start` - starts dev server
2. open that dummy app on your phone
3. ???
4. PROFIT!


Also, when you install new cordova plugins, run `node ./steal-cordova-scripts.js`, so those plugins are copied to /public. 


# Some details for curious:
We use *create-react-app* to create simple dev server, which under the hood configures webpack so it automatically compiles /src to the form that browsers can digest, also supports ‘hot reloading’ – when you make any change to /src, it all instantly shows in browser. 

## Hot reloading on cordova
The way hot reloading on cordova works: Cordova puts an app on your phone, and that app instead of rendering local index.html, loads http://192.168.x.x:3000, where react server is hosted. ~~When the time will come to make a production build, I’ll make a little script for that. (For now app only opens when your phone is in your local network and the dev server is started on your desktop)~~ 

## Production build
To make a full-fledged production build (all resources bundled together in .apk or .whatever-ios-uses)) do this:

1. `node make-cordova-build.js platform=android` or `platform=ios` - makes a build and uploads it on a device. Add `--skip-upload` to just build stuff, `--only-upload` to skip build and only upload.
2. *optional* Raise your eyebrows and say: *Hey, that's pretty good*

## Native functions on dev server
In order to use native functions, such as camera, we gotta put < script src='cordova.js'> on the page. The problem is that 'cordova.js' is platform-specific (different for ios,android,browser), but alas, our server sends the same index.html from /public (where we would put < script src='cordova.js'>) to all platforms! Damn it! The solution is to put the logic of adding cordova <script> in */src/index.js*, where we can determine platform on the client himself and load cordova.js for his specific platform. Yee. 
So now we can browse our dev server from android, ios, browser and use native functions like there’s no tomorrow!
Also you need to run *steal-cordova-scripts.js* to steal cordova.js and other necessary files to */public*. 

## Camera in browser
Camera on browser doesn’t work (at least on Chrome), this is due to the fact that cordova camera plugin is as fucking old as your grandma. URL.createObjectURL doesn’t accept MediaStream anymore in modern browsers, and that’s a shame. I found a way to fix it, you gotta replace `line 105` in */platforms/browser/platform_www/plugins/cordova-plugin-camera/src/browser/CameraProxy.js* from `video.src = window.URL.createObjectURL(localMediaStream);` to `video.srcObject = localMediaStream;` and run in command line `node ./steal-cordova-scripts.js` again. But if you don’t care about browser camera, don’t bother.
